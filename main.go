package main

import (
	"fmt"
	"os"

	"flag"
	"golang.org/x/crypto/bcrypt"
)

var licenseString = `
    pass-hash
    Copyright (C) 2018  Scott Snyder

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

`

func checkPass(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	if err != nil {
		return false
	} else {
		return true
	}
}

func hashPass(password string, cost int) string {
	sh, err := bcrypt.GenerateFromPassword([]byte(password), cost)
	if err != nil {
		fmt.Printf("err: %s\n", err)
		os.Exit(1)
	}
	return string(sh)
}

func main() {
	var cost int
	var operationVar string
	var hashVar string
	var passwordVar string
	var verbose bool
	var version bool
	var license bool
	var costValue bool
	flag.BoolVar(&costValue, "cost-value", false, "return the cost value of a hash")
	flag.BoolVar(&license, "license", false, "show license and exit")
	flag.BoolVar(&version, "version", false, "show version info and exit")
	flag.BoolVar(&verbose, "verbose", false, "be verbose")
	flag.StringVar(&passwordVar, "password", "", "password")
	flag.StringVar(&hashVar, "hash", "", "hash '{string}'")
	flag.StringVar(&operationVar, "operation", "", "operation hash/compare")
	flag.IntVar(&cost, "cost", 10, "cost 10-31")
	flag.Parse()
	if costValue {
		costValeInt, err := bcrypt.Cost([]byte(hashVar))
		if err != nil {
			fmt.Printf("err : %s", err)
			os.Exit(1)
		}
		fmt.Printf("%d\n", costValeInt)
	}
	if license {
		fmt.Printf("%s\n", licenseString)
		os.Exit(0)
	}
	if version {
		fmt.Println("  pass-hash v-0.01a\n    ")
		if verbose {
			fmt.Println("    https://bitbucket.org/ul7r41337h4x0r/pass-hash\n\n")
		}
		os.Exit(0)
	}
	if operationVar == "hash" {
		fmt.Printf("%s\n", hashPass(passwordVar, cost))
	}
	if operationVar == "compare" {
		if checkPass(passwordVar, hashVar) {
			if verbose {
				fmt.Println("match")
			}
			os.Exit(0)
		} else {
			if verbose {
				fmt.Printf("error : hash : %s did not match password: %s", hashVar, passwordVar)
            }
			os.Exit(1)
		}
	}
}
